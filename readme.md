# Todolist

## Sommaire
- [Todolist](#todolist)
  - [Sommaire](#sommaire)
    - [Description](#description)
    - [Getting Started](#getting-started)
      - [Installation](#installation)
      - [R�pertoire](#r%c3%a9pertoire)
      - [IDE](#ide)
    - [Configuration avanc�e](#configuration-avanc%c3%a9e)
    - [Extensions / Personnalisation](#extensions--personnalisation)
    - [Cr�dits](#cr%c3%a9dits)

***

### Description
Ce projet a pour but de cr�er une `todo list` interactive. 
Ce projet personnel est en cours de r�alisation, sa port�e est uniquement �ducative.

De nombreuses features sont absentes du projet et seront ajout�es au fur-et-�-mesure de sont avancement.
Par exemple : des tests unitaires, Vue.js pour la partie front-end, d�finir un niveau de log (debug en d�veloppement et info en production) etc.

Ce projet est d�compos� est architecture N-tiers de telle sorte � pouvoir faciliter la compr�hension et la "propret�" du projet.

***

### Getting Started
#### Installation

Installer Node.js (version : Latest -- **ONLY LTS**) : https://nodejs.org/fr/  

Extraire l'archive todolist.zip dans le r�pertoire de votre choix.

*Remarque : Il est pr�f�rable de conserver le m�me nom de dossier pour votre r�pertoire de destination c.�.d. **todolist**).*

Rendez-vous dans votre projet et ex�cuter la commande suivante � l'aide d'un terminal
(Cette commande t�l�charge toutes les d�pendances n�cessaires � votre projet) :
    
    npm install

#### R�pertoire
| R�pertoire        | Description |
| ------            | ----------- |
| dist              | Code g�n�r� depuis Typescript vers Javascript (Code ex�cut� en production). |
| node_modules      | D�pendances du projet (T�l�chargement automatique � l'initialisation du projet). |
| src               | Sources du projet ainsi que le point d'entr�e de l'application server.ts. |
| src/config        | Configuration du projet (connexion � la base de donn�es, token etc.). |
| src/controllers   | Controllers de l'application (Logique m�tier, algorithmie etc.). |
| src/models        | Requ�tes SQL. |
| src/public        | Code css, mise en page. |
| src/public/assets | Images, musiques, video etc. |
| src/types         | Dossier de d�p�ts des .json permettant l'ajout de types � Typescript. |
| test              | Fichier de tests (Jest). |
| view              | Front-end du projet (HTML, Vue.js). |

#### IDE
Nous recommandons vivement l'utilisation de l'IDE Visual Studio Code.
  * VSCode est disponible � l'adresse suivante : https://code.visualstudio.com/

**Nous n'utilisons pas de d�pendances particuli�res au sein de VSCode**

### Configuration avanc�e

Ex�cuter le projet en mode **"D�veloppement"** :

    npm run dev

*D�marre le projet � l'adresse suivante : http://localhost:3000/*

**Features disponibles dans ce mode**
1. Live-Reload
2. Run Test unitaires
3. Debug 

Ex�cuter le projet en mode **"Production"** :

    npm run prod

*Compile et ex�cute le code Javascript pr�sent dans le dossier **dist***

**Features disponibles dans ce mode** 
1. ~~Live-Reload~~
2. ~~Run Test unitaires~~
3. ~~Debug~~

**Point d'entr�e de l'application** 
``` js
import express from 'express';
import path from 'path';

const app: express.Application = express();
const port: Number = 3000;
const view_index: string = '/view/index.html';


app.get('/', (req, res) => {
    res.sendFile(view_index, { root: path.join(__dirname, '../') });
});

app.listen(port, () => {
    console.log(`Listenning on localhost:${port}`);
});
```

**Package.json**

Fichier dans lequel d�clarer les d�pendances du projet, les scripts d'ex�cution (npm run ***), le nom du projet,
la description du projet et sa version.

### Extensions / Personnalisation
Nous utilisons ***fi�rement*** :
* Typescript : Langage principal de conception de ce projet.
* Javascript : Langage d'ex�cution.
* Node.js : Environnement d'ex�cution Javascript (� terme, nous souhaitons migrer vers Deno.js).
* Express.js : Framework simplifiant la cr�ation d'une application web (� terme, nous souhaitons nous d�sengager de ce framework).
* Jest : Framework de test
* ***Soon --> Vue.js*** : Librairie Front-end

### Cr�dits
Nous remercions chaleureusement :
* Visual Studio Code : https://code.visualstudio.com/
* Typescript : https://www.typescriptlang.org/
* Javascript : https://www.ecma-international.org/publications/standards/Ecma-262.htm
* Node.js : https://nodejs.org/fr/ 
* Express.js : https://expressjs.com/fr/ 
* Jest : https://jestjs.io/
* Vue.js : https://vuejs.org/
* Toutes les personnes participants � ce projet !