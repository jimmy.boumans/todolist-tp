"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var app = express();
var port = 3000;
var view_index = '/view/index.html';
app.get('/', function (req, res) {
    res.sendFile(__dirname + view_index);
});
app.listen(port, function () {
    console.log("Listenning on localhost:" + port);
});
