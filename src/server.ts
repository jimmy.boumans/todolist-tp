import express from 'express';
import path from 'path';

const app: express.Application = express();
const port: Number = 3000;
const view_index: string = '/view/index.html';


app.get('/', (req, res) => {
    res.sendFile(view_index, { root: path.join(__dirname, '../') });
});

app.listen(port, () => {
    console.log(`Listenning on localhost:${port}`);
});


/*
import * as http from 'http';

const port: Number = 3000;
const server: http.Server = http.createServer();

server.on('req', (req: any, res: any) => {
    if(req.url === '/'){
        res.end("bonjour /");
    } else {
        res.end("Vous n'�tes pas sur la bonne ressource...");
    }
});

server.listen(port, () => {
    console.log(`Listenning on localhost:${port}`);
});
*/